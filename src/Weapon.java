import java.util.Random;

public class Weapon {
    static Random rand = Main.rand;
    int damage;
    boolean zoom = false;
    Type type;

    Weapon() {
        this.type = Type.values()[rand.nextInt(Type.values().length)];
        if (type == Type.AR) {
            this.damage = 10;
        } else {
            this.damage = 20;
            this.zoom = rand.nextBoolean();
        }
    }

    enum Type {AR, SR}
}