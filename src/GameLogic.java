public class GameLogic {
    public static void play(Soldier[] side1, Soldier[] side2) {
        int round = 0;
        int n1 = choose(side1);
        int n2 = choose(side2);
        while (alive(side1) && alive(side2)) {
            Soldier s1 = side1[n1];
            Soldier s2 = side2[n2];
            System.out.println("\n\t\t\tround" + round++);
            show(side1, n1, 1);
            show(side2, n2, 2);
            s2.hp -= s1.damage();
            if (s2.hp <= 0) {
                s2.alive = false;
                if (alive(side2))
                    n2 = choose(side2);
                continue;
            }
            s1.hp -= s2.damage();
            if (s1.hp <= 0) {
                s1.alive = false;
                if (alive(side1))
                    n1 = choose(side1);
            }
        }
        show(side1, -1, 1);
        show(side2, -1, 2);
        System.out.println("\nside " + (alive(side1) ? 1 : 2) + " won!");
    }

    private static int choose(Soldier[] side) {
        int r;
        do r = Main.rand.nextInt(side.length);
        while (!side[r].alive);
        return r;
    }

    private static void show(Soldier[] side, int shooter, int n) {
        System.out.println("\nside" + n + " :");
        System.out.println("soldier\t\thp\t\trifle\tcaliber\tzoom");
        for (int i = 0; i < side.length; i++) {
            Soldier s = side[i];
            System.out.printf("%sS%d\t\t%d\t\t%s\t\t%dMM\t\t%c\n",
                              shooter == i ? "->" : "  ", i, s.hp >= 0 ? s.hp : 0, s.weapon.type, s.cal,
                              s.weapon.zoom ? '+' : '-'
            );
        }
    }

    private static boolean alive(Soldier[] side) {
        for (Soldier soldier : side)
            if (soldier.alive)
                return true;
        return false;
    }
}
