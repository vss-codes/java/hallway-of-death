import java.util.Random;
import java.util.Scanner;

public class Main {
    public static Random rand = new Random();

    public static void main(String[] args) {

        System.out.println("enter the length of the hallway i.e. number of soldiers in each side");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        Soldier[] side1 = new Soldier[n];
        Soldier[] side2 = new Soldier[n];
        for (int i = 0; i < n; i++) {
            side1[i] = new Soldier();
            side2[i] = new Soldier();
        }
        int n1 = Main.rand.nextInt(side1.length);
        int n2 = Main.rand.nextInt(side2.length);
        GameLogic.play(side1, side2);
    }
}
