import java.util.Random;

public class Soldier {
    static Random rand = Main.rand;
    int hp = 100;
    boolean alive = true;
    int cal;
    Weapon weapon = new Weapon();

    Soldier() {
        this.cal = rand.nextInt(2) * 2 + 5;
    }

    public double damage() {
        int rate = this.weapon.type == Weapon.Type.AR ? 50 : 60;
        if (this.weapon.zoom) rate += rand.nextInt(10) + 5;
        int damage = this.weapon.damage;
        if (this.cal == 7) {
            rate -= 10;
            damage += 10;
        } else {
            rate += 15;
        }
        if (rand.nextInt(100) <= rate) {
            return damage;
        }
        return 0;
    }
}